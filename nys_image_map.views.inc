<?php

/**
 * @file
 * Defines the View Style Plugins for NYS Image Map module.
 */

/**
 * Implements hook_views_plugins().
 */
function nys_image_map_views_plugins() {
  return array(
    'module' => 'nys_image_map',
    'style' => array(
      'nys_image_map' => array(
        'title' => t('NYS Image Map'),
        'help' => t('Display the results as an image map.'),
        'handler' => 'views_plugin_style_nys_image_map',
        'path' => drupal_get_path('module', 'nys_image_map'),
        'theme' => 'views_view_nys_image_map',
        'uses row plugin' => TRUE,
        'uses row class' => FALSE,
        'uses grouping' => FALSE,
        'uses options' => TRUE,
        'uses fields' => TRUE,
        'type' => 'normal',
        // 'help topic' => 'style-list',
        'even empty' => TRUE,
      ),
    ),
  );
}
