/**
 * @file
 * JavaScript for NYS Image Map.
 */

(function ($) {
  $(document).ready(function(){
    function closeTheClickDialog(){
      $('.map-region-box-popup').dialog("close");
    }
    // POP-UPS ------------------------------------------------------------------------------------->
    // click a county
    $('#countyImageMap area').click(function(){
      //turn off click when there's a box already open
      //if (!($('.map-region-box-popup').is(":visible"))){
        // close any open dialogs
        $('.map-region-box-popup').dialog("close");
        $('.map-region-box-hover').dialog("close");
        // construct the id of the popup box
        var thisSelectedCountyDialog = 'map-region-box-popup-' + $(this).attr("id");
        // if the popup box exists
        if ($('#' + thisSelectedCountyDialog).length > 0){
          // make a dialog out of it
          $('#' + thisSelectedCountyDialog).dialog();
          // use the h3 as a title for the dialog box
          $('#' + thisSelectedCountyDialog).dialog("option", "title", $('#' + thisSelectedCountyDialog + ' h3').html());
          // add a class to the popup box
          $('#' + thisSelectedCountyDialog).dialog("option", "dialogClass", "popup-box-open");
          // change size
          $('#' + thisSelectedCountyDialog).dialog("option", "width", 550);
          // because we are lazy, now we have to re-center
          $('#' + thisSelectedCountyDialog).dialog('option', 'position', 'center');
        }
      //}
      // make sure the a tag's default event doesn't fire
      return false;
    });
    // HOVERS -------------------------------------------------------------------------------------->
    // hover over a county
    $('#countyImageMap area').hover(function(event){
      /* turn off hovers when a pop-up is active */
      if (!($('.map-region-box-popup').is(":visible"))){
        // close any open hover dialogs
        $('.map-region-box-hover').dialog("close");
        // construct the id of the tooltip
        var thisSelectedCountyDialog = 'map-region-box-hover-' + $(this).attr("id");
        // if the tooltip exists
        if ($('#' + thisSelectedCountyDialog).length > 0){
          // make a dialog out of it
          $('#' + thisSelectedCountyDialog).dialog();
          // SETTINGS - This format makes it easy to add or remove things. <<<<<<<<<<<<<<<<
          // use the h3 as a title for the dialog box
          $('#' + thisSelectedCountyDialog).dialog("option", "title", $('#' + thisSelectedCountyDialog + ' h3').html());
          // set the position of the dialog based on where the mouse is
          $('#' + thisSelectedCountyDialog).dialog("option", "position", [event.clientX + 60,event.clientY - 40]);
          // allow the dialog to appear outside the boundaries of the screen
          $('#' + thisSelectedCountyDialog).dialog("widget").draggable("option","containment","none");
          // add a class to the hover box
          $('#' + thisSelectedCountyDialog).dialog("option", "dialogClass", "hover-box-open");
          // add an option to the dialog to turn off collision detection
          $.extend($.ui.dialog.prototype.options.position, { collision: 'none' });
          // turn off collision detection because of an issue regarding the popup opening and appearing under the cursor
          // this does, however, cause the dialog to appear outside the perimeter of the page if someone is using a low resolution.
          $('#' + thisSelectedCountyDialog).dialog("option", "collision", "none");
          // turn off resize
          $('#' + thisSelectedCountyDialog).dialog("option", "resizable", false);
          // change size
          $('#' + thisSelectedCountyDialog).dialog("option", "width", 225);
          //  >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
        }
      }
    });
    // on mouseout from an area...
    $('#countyImageMap area').mouseout(function(){
      // close the dialog
      $('.map-region-box-hover').dialog("close");
    });
  });
}(jQuery));
