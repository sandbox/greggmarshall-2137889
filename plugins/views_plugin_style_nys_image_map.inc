<?php

/**
 * @file
 * nys_image_map style plugin.
 */

/**
 * Style plugin to render a map.
 *
 * @ingroup views_style_plugins
 */
class views_plugin_style_nys_image_map extends views_plugin_style {
  /**
   * Set default options.
   */
  function option_definition() {
    $options = parent::option_definition();
    $module_path = drupal_get_path('module', 'nys_image_map');

    $options['imagemap_image_location'] = array('default' => $module_path . '/imagemap/nys-counties-large.png');
    $options['imagemap_imagemap_location'] = array('default' => $module_path . '/imagemap/nycountyimagemap.inc');
    $options['area_id'] = array('default' => '');
    $options['hover_title'] = array('default' => '');
    $options['hover_body'] = array('default' => '');
    $options['click_title'] = array('default' => '');
    $options['click_body'] = array('default' => '');

    return $options;
  }

  /**
   * Get user options.
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $field_options = array();
    $field_options_required = array();
    $field_options['none'] = 'None';
    $fields = $this->display->handler->get_handlers('field');
    foreach ($fields as $id => $handler) {
      $field_options[$id] = $handler->ui_name(FALSE);
      $field_options_required[$id] = $handler->ui_name(FALSE);
    }

    $form['imagemap_image_location'] = array(
      '#title' => t('Image map image'),
      '#type' => 'textfield',
      '#size' => 60,
      '#description' => t('The path of the image that will be used in the image map.'),
      '#default_value' => $this->options['imagemap_image_location'],
      '#required' => TRUE,
    );
    $form['imagemap_imagemap_location'] = array(
      '#title' => t('Image map definition file'),
      '#type' => 'textfield',
      '#description' => t('The path of the image map definition file that turns the image into an clickable image map.'),
      '#default_value' => $this->options['imagemap_imagemap_location'],
      '#required' => TRUE,
    );
    $form['area_id'] = array(
      '#title' => t('Area ID Field'),
      '#description' => empty($field_options) ? t("The field's format must be text.  You must be using the fields row style and have added fields to the view to use this option.") : t("The field's format must be text."),
      '#type' => 'select',
      '#options' => $field_options_required,
      '#default_value' => $this->options['area_id'],
      '#process' => array('ctools_dependent_process'),
    );
    $form['hover_title'] = array(
      '#title' => t('Hover Title'),
      '#description' => empty($field_options) ? t("The field's format must be text.  You must be using the fields row style and have added fields to the view to use this option.") : t("The field's format must be text."),
      '#type' => 'select',
      '#options' => $field_options_required,
      '#default_value' => $this->options['hover_title'],
      '#process' => array('ctools_dependent_process'),
    );
    $form['hover_body'] = array(
      '#title' => t('Hover Body'),
      '#description' => empty($field_options) ? t("The field's format must be text.  You must be using the fields row style and have added fields to the view to use this option.") : t("The field's format must be text."),
      '#type' => 'select',
      '#options' => $field_options,
      '#default_value' => $this->options['hover_body'],
      '#process' => array('ctools_dependent_process'),
    );
    $form['click_title'] = array(
      '#title' => t('Click Title'),
      '#description' => empty($field_options) ? t("The field's format must be text.  You must be using the fields row style and have added fields to the view to use this option.") : t("The field's format must be text."),
      '#type' => 'select',
      '#options' => $field_options_required,
      '#default_value' => $this->options['click_title'],
      '#process' => array('ctools_dependent_process'),
    );
    $form['click_body'] = array(
      '#title' => t('Click Body'),
      '#description' => empty($field_options) ? t("The field's format must be text.  You must be using the fields row style and have added fields to the view to use this option.") : t("The field's format must be text."),
      '#type' => 'select',
      '#options' => $field_options,
      '#default_value' => $this->options['click_body'],
      '#process' => array('ctools_dependent_process'),
    );
  }

  /**
   * Render the display in this style.
   */
  function render() {
    // Let's make sure the individual fields are rendered.
    $this->render_fields($this->view->result);
    $output = '';
    $output .= '<img src="/' . $this->options['imagemap_image_location'] . '" alt="image map" id="nyCounties" usemap="#countyImageMap">';
    $output .= file_get_contents($this->options['imagemap_imagemap_location']);
    foreach ($this->view->result as $row_index => $row) {
      $area_id = '';
      if (!empty($this->rendered_fields[$row_index][$this->options['area_id']]) && ($this->options['area_id'] != 'none')) {
        $area_id = $this->rendered_fields[$row_index][$this->options['area_id']];
      }
      $hover_title = '';
      if (!empty($this->rendered_fields[$row_index][$this->options['hover_title']]) && ($this->options['hover_title'] != 'none')) {
        $hover_title = $this->rendered_fields[$row_index][$this->options['hover_title']];
      }
      $hover_body = '';
      if (!empty($this->rendered_fields[$row_index][$this->options['hover_body']]) && ($this->options['hover_body'] != 'none')) {
        $hover_body = $this->rendered_fields[$row_index][$this->options['hover_body']];
      }
      $click_title = '';
      if (!empty($this->rendered_fields[$row_index][$this->options['click_title']]) && ($this->options['click_title'] != 'none')) {
        $click_title = $this->rendered_fields[$row_index][$this->options['click_title']];
      }
      $click_body = '';
      if (!empty($this->rendered_fields[$row_index][$this->options['click_body']]) && ($this->options['click_body'] != 'none')) {
        $click_body = $this->rendered_fields[$row_index][$this->options['click_body']];
      }
      $output .= '<div id="map-region-box-' . $area_id . '" class="map-region-box-wrap">' . "\n";
      $output .= '  <div class="map-region-box-hover"  id="map-region-box-hover-' . $area_id . '">' . "\n";
      $output .= '    <h3>' . $hover_title . '</h3>' . "\n";
      if (!empty($hover_body)) {
        $output .= '    <div class="map-region-box-hover-content">' . "\n";
        $output .= '      ' . $hover_body . "\n";
        $output .= '    </div>' . "\n";
      }
      $output .= '  </div>' . "\n";
      $output .= '  <div class="map-region-box-popup" id="map-region-box-popup-' . $area_id . '">' . "\n";
      $output .= '    <h3>' . $click_title . '</h3>' . "\n";
      if (!empty($click_body)) {
        $output .= '    <div class="map-region-box-popup-content">' . "\n";
        $output .= '      ' . $click_body . "\n";
        $output .= '    </div>' . "\n";
      }
      $output .= '  </div>' . "\n";
      $output .= '</div>' . "\n";

    }
    return $output;
  }
}
